#include <cmsis_compiler.h> 
#include <arm_math.h> 
#include <SPI.h> 
#include <WiFi.h> 
#include <PubSubClient.h> 
#include<stdio.h> 
#define NUM_SAMPLES 192 
#define BLOCK_SIZE 64 
#define NUM_TAPS 4 
//init arrays for read in data and filtered data 
float32_t ecgdata[NUM_SAMPLES]; // A global array for the input data 
float32_t filtered_data[300]; // A global array for the low-pass filtered output data 
uint32_t numBlocks = NUM_SAMPLES/BLOCK_SIZE; 
uint32_t blockSize = BLOCK_SIZE; 
// Initialise FIR filter structure 
const float32_t firCoeffs32[NUM_TAPS] = { +0.04694066f, +0.45305934f, +0.45305934f, +0.04694066f}; 
// Coefs from 4-tap FIR filter 
 float32_t firStateF32[NUM_TAPS + BLOCK_SIZE - 1]; // Declare state buffer 
arm_fir_instance_f32 S; 
char ssid[] = "BsDx-bWF6enk"; 
char password[] = "mazzy0593"; 
#define TOKEN "BBFF-y110gNPyp7sWhFoA9Nz8KDIF756Efn" // Ubidots' TOKEN 
#define MQTT_CLIENT_NAME "myecgsensor" 
char server[] = "industrial.api.ubidots.com"; 
#define VARIABLE_LABEL "ecg" // Assing the variable label 
#define DEVICE_LABEL "MSP432" // Assig the device label 29 
char payload[100];//holds our data to be published 
char topic[150];//topic data is published 
// Space to store values to send 
char str_sensor[10]; //buffer to append our data to be sent to server 
WiFiClient wifiClient;//instance of class wifiClient 
PubSubClient client(server, 1883, callback, wifiClient);//defines mqtt connection 24 
void callback(char* topic, byte* payload, unsigned int length) { 
 //callback function when data is received 
 char p[length + 1]; 
 memcpy(p, payload, length); 
 //p[length] = NULL; 
 Serial.write(payload, length); 
 Serial.println(topic); 
} 
void setup() { 
 pinMode(13, INPUT_PULLUP); // Setup for leads off detection LO + 
 pinMode(12, INPUT_PULLUP); // Setup for leads off detection LO - 
 Serial.begin(9600); 
 Serial.begin(115200);//enabling serial communication at 115200 baud rate 
 //pinMode(SENSOR, INPUT);//defines our ecg pin as input 
 // attempt to connect to Wifi network: 
 Serial.print("Attempting to connect to Network named: "); 
 // print the network name (SSID); 
 Serial.println(ssid); 
 // Connect to WPA/WPA2 network. Change this line if using open or WEP network: 
 WiFi.begin(ssid, password); //connect to wifi router 
 while ( WiFi.status() != WL_CONNECTED) {//waits till we're connected 
 // print dots while we wait to connect 
 Serial.print("."); 
 delay(300); 
 } 
 Serial.println("\nYou are connected to the network"); 
 Serial.println("Waiting for an ip address"); 
 while (WiFi.localIP() == INADDR_NONE) { 
 // print dots while waiting for an IP addresss 
 Serial.print("."); 
 delay(300); 
 } 
 Serial.println("\nIP Address obtained"); 
 // Connected and obtained an IP address. 
 // Print the WiFi status. 
 printWifiStatus();//prints out IP address allocated and signal strength 
} 
void reconnect() {//manages reconnection/connection to server if we're disconnected 
 // Loop until we're reconnected 
 while (!client.connected()) { 
 Serial.println("Attempting MQTT connection..."); 
 // Attemp to connect 
 if (client.connect(MQTT_CLIENT_NAME, TOKEN, "")) {//sends MQTT connect message 
 Serial.println("Connected"); 
 } else { 30 
 Serial.print("Failed, rc="); 
 Serial.print(client.state()); 
 Serial.println(" try again in 2 seconds"); 
 // Wait 2 seconds before retrying 
 delay(2000); 
 } 
 } 
} 
unsigned long lastConnTime = 0; // Track the last update time 25 
bool TimeSpent(unsigned long prevTime, unsigned long value) {//returns true when the comparision between 
current time is greater that time to be checked 
 bool returnVal = false; 
 returnVal = ((millis() - prevTime) > value) ? true : false;//use of a ternery operator 
 return returnVal; 
} 
void printWifiStatus() { 
 // print the SSID of the network you're attached to: 
 Serial.print("SSID: "); 
 Serial.println(WiFi.SSID()); 
 // print your WiFi IP address: 
 IPAddress ip = WiFi.localIP(); 
 Serial.print("IP Address: "); 
 Serial.println(ip); 
 // print the received signal strength: 
 long rssi = WiFi.RSSI(); 
 Serial.print("signal strength (RSSI):"); 
 Serial.print(rssi); 
 Serial.println(" dBm"); 
} 
float32_t * detectQRS(float32_t ecgdata[300]){ 
 //D1 D2 and difarr are arrays for working on the ecgdata values 
 float32_t *dif; 
 float32_t peaks[300]; 
 //length of the ECG data array 
 int thresh = getThreshold(ecgdata); 
 int length = 200; 
 //do double square calculations & get 3% threshold 
 dif = doublesquare(ecgdata); 
 thresh = getThreshold(dif); 
 //Serial.println(difarr[j]); 
 //compare dif array values to threshold 
 for(int i = 0;i<length;i++){ 
 if(dif[i] > thresh){ 
 if(dif[i] > 0){ 
 peaks[i] = dif[i]; 
 } 
 Serial.println(peaks[i]); 
 } 
